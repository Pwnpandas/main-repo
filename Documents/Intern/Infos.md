# Here are some infos on how tos for common tasks we do

## How to encrypt a file with our public key:

The key was generated with the following command:
`openssl genrsa -out private.key 8912` (No password. Igo always forgets passwords 🙄)
Now extract the public key from the private key:
`openssl rsa -in private.key -pubout -out public.key`.

Encryption: `openssl rsautl -encrypt -pubin -inkey public.key -in test_file_plain.txt  -out test_file_enrypted.txt`.
Decryption: `openssl rsautl -decrypt -inkey private.key -in test_file_enrypted.txt  -out test_file_plain.txt`.

@outteam: You can check whether the private key you are using is correct, by decrypting the `test_file_encrypted`. If the content is the same as `test_file_plain.txt` the key is correct.
