# The PwnPandas

We are the almighty PwnPandas and are here to pwn you 🐼.

We are a family of hacking pandas that are truly luminary on in that area.

And we are also lazy..... (Thats what pandas do 🎋+😴).

## What you find here

Everything we want to share with the world to show our dominance.

Here we store all our important information. Everything that should be kept secret is encrypted with our public key. You will never be able to break our RSA key.

In case you want to send us some info in a secure way you can find out public key under `Documents/Intern/Encryption`.
Use `openssl rsautl -encrypt -pubin -inkey public.key -in <your_file> -out <your_file_encrypted>`.
